# README #

I'm having trouble deploying my EE7 JAX-RS-app to RedHat JBoss EAP servers.

Different server versions refuse to deploy my .war-file based heavily on its physical filename.

To reproduce: Clone this repo, build it using "mvn clean package" and copy the resulting .war file into Jboss' standalone/deployments/ folder, using its own or a different file name.

Or just use the provided war file in /bin/app.war.

A pre-built war file is included in the "bin" folder.

All my tests have been performed with (actually or pretty much) original servers and configs as downloaded from RedHat, see the "config" folder, from a stopped server with a clean standalone/deployments folder with only the one war file to be tested copied in and started /bin/standalone.sh without any arguments.

Tested so far: (by filename)

## EAP 7.1.0
* app.war works
* app-1.war works
* app-1-2.war DOESN'T work
* app-1-2-3.war works
* app-1-2-3-4.war works
* crisrest-2.6-SNAPSHOT.war works
* crisrest-2.7-SNAPSHOT DOESN'T work
* crisrest-2.7.war works
* welderror-0.1-SNAPSHOT.war DOESN'T work

## EAP 7.2.2
* app.war DOESN'T work
* app-1.war works
* app-1-2.war DOESN'T work
* app-1-2-3.war sometimes works
* app-1-2-3-4.war works
* crisrest-2.6-SNAPSHOT.war DOESN'T work
* crisrest-2.7-SNAPSHOT works
* crisrest-2.7.war sometimes works
* welderror-0.1-SNAPSHOT.war works

## EAP 7.3.0
* app.war DOESN'T work
* app-1.war works
* app-1-2.war DOESN'T work
* app-1-2-3.war sometimes works
* app-1-2-3-4.war works
* crisrest-2.6-SNAPSHOT.war DOESN'T work
* crisrest-2.7-SNAPSHOT.war works
* crisrest-2.7.war sometimes works
* welderror-0.1-SNAPSHOT.war works

## Example log messages

	21:38:47,918 INFO  [org.jboss.resteasy.resteasy_jaxrs.i18n] (ServerService Thread Pool -- 61)  RESTEASY002225: Deploying javax.ws.rs.core.Application: class com.sachsezhang.TheApp$Proxy$_$$_WeldClientProxy
	21:38:47,921 ERROR [org.jboss.msc.service.fail] (ServerService Thread Pool -- 61)  MSC000001: Failed to start service jboss.undertow.deployment.default-server.default-host./welderror: org.jboss.msc.service.StartException in service jboss.undertow.deployment.default-server.default-host./welderror: org.jboss.weld.context.ContextNotActiveException: WELD-001303: No active contexts for scope type javax.enterprise.context.RequestScoped
			at org.wildfly.extension.undertow.deployment.UndertowDeploymentService$1.run(UndertowDeploymentService.java:84) [wildfly-undertow-7.1.0.GA-redhat-11.jar:7.1.0.GA-redhat-11]
			at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511) [rt.jar:1.8.0_242]
			at java.util.concurrent.FutureTask.run(FutureTask.java:266) [rt.jar:1.8.0_242]
			at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149) [rt.jar:1.8.0_242]
			at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624) [rt.jar:1.8.0_242]
			at java.lang.Thread.run(Thread.java:748) [rt.jar:1.8.0_242]
			at org.jboss.threads.JBossThread.run(JBossThread.java:320)
	Caused by: org.jboss.weld.context.ContextNotActiveException: WELD-001303: No active contexts for scope type javax.enterprise.context.RequestScoped
			at org.jboss.weld.manager.BeanManagerImpl.getContext(BeanManagerImpl.java:705)
			at org.jboss.weld.bean.ContextualInstanceStrategy$DefaultContextualInstanceStrategy.getIfExists(ContextualInstanceStrategy.java:89)
			at org.jboss.weld.bean.ContextualInstanceStrategy$CachingContextualInstanceStrategy.getIfExists(ContextualInstanceStrategy.java:164)
			at org.jboss.weld.bean.ContextualInstance.getIfExists(ContextualInstance.java:63)
			at org.jboss.weld.bean.proxy.ContextBeanInstance.getInstance(ContextBeanInstance.java:83)
			at org.jboss.weld.bean.proxy.ProxyMethodHandler.getInstance(ProxyMethodHandler.java:125)
			at com.sachsezhang.TheApp$Proxy$_$$_WeldClientProxy.getClasses(Unknown Source)
			at org.jboss.resteasy.spi.ResteasyDeployment.processApplication(ResteasyDeployment.java:526)
			at org.jboss.resteasy.spi.ResteasyDeployment.registration(ResteasyDeployment.java:372)
			at org.jboss.resteasy.spi.ResteasyDeployment.start(ResteasyDeployment.java:258)
			at org.jboss.resteasy.plugins.server.servlet.ServletContainerDispatcher.init(ServletContainerDispatcher.java:120)
			at org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher.init(HttpServletDispatcher.java:36)
			at io.undertow.servlet.core.LifecyleInterceptorInvocation.proceed(LifecyleInterceptorInvocation.java:117)
			at org.wildfly.extension.undertow.security.RunAsLifecycleInterceptor.init(RunAsLifecycleInterceptor.java:78) [wildfly-undertow-7.1.0.GA-redhat-11.jar:7.1.0.GA-redhat-11]
			at io.undertow.servlet.core.LifecyleInterceptorInvocation.proceed(LifecyleInterceptorInvocation.java:103)
			at io.undertow.servlet.core.ManagedServlet$DefaultInstanceStrategy.start(ManagedServlet.java:250)
			at io.undertow.servlet.core.ManagedServlet.createServlet(ManagedServlet.java:133)
			at io.undertow.servlet.core.DeploymentManagerImpl$2.call(DeploymentManagerImpl.java:565)
			at io.undertow.servlet.core.DeploymentManagerImpl$2.call(DeploymentManagerImpl.java:536)
			at io.undertow.servlet.core.ServletRequestContextThreadSetupAction$1.call(ServletRequestContextThreadSetupAction.java:42)
			at io.undertow.servlet.core.ContextClassLoaderSetupAction$1.call(ContextClassLoaderSetupAction.java:43)
			at org.wildfly.extension.undertow.security.SecurityContextThreadSetupAction.lambda$create$0(SecurityContextThreadSetupAction.java:105) [wildfly-undertow-7.1.0.GA-redhat-11.jar:7.1.0.GA-redhat-11]
			at org.wildfly.extension.undertow.deployment.UndertowDeploymentInfoService$UndertowThreadSetupAction.lambda$create$0(UndertowDeploymentInfoService.java:1508) [wildfly-undertow-7.1.0.GA-redhat-11.jar:7.1.0.GA-redhat-11]
			at org.wildfly.extension.undertow.deployment.UndertowDeploymentInfoService$UndertowThreadSetupAction.lambda$create$0(UndertowDeploymentInfoService.java:1508) [wildfly-undertow-7.1.0.GA-redhat-11.jar:7.1.0.GA-redhat-11]
			at org.wildfly.extension.undertow.deployment.UndertowDeploymentInfoService$UndertowThreadSetupAction.lambda$create$0(UndertowDeploymentInfoService.java:1508) [wildfly-undertow-7.1.0.GA-redhat-11.jar:7.1.0.GA-redhat-11]
			at org.wildfly.extension.undertow.deployment.UndertowDeploymentInfoService$UndertowThreadSetupAction.lambda$create$0(UndertowDeploymentInfoService.java:1508) [wildfly-undertow-7.1.0.GA-redhat-11.jar:7.1.0.GA-redhat-11]
			at io.undertow.servlet.core.DeploymentManagerImpl.start(DeploymentManagerImpl.java:578)
			at org.wildfly.extension.undertow.deployment.UndertowDeploymentService.startContext(UndertowDeploymentService.java:100) [wildfly-undertow-7.1.0.GA-redhat-11.jar:7.1.0.GA-redhat-11]
			at org.wildfly.extension.undertow.deployment.UndertowDeploymentService$1.run(UndertowDeploymentService.java:81) [wildfly-undertow-7.1.0.GA-redhat-11.jar:7.1.0.GA-redhat-11]
			... 6 more
	
	21:38:47,936 ERROR [org.jboss.as.controller.management-operation] (Controller Boot Thread)  WFLYCTL0013: Operation ("deploy") failed - address: ([("deployment" => "welderror-0.1-SNAPSHOT.war")]) - failure description: {"WFLYCTL0080: Failed services" => {"jboss.undertow.deployment.default-server.default-host./welderror" => "org.jboss.weld.context.ContextNotActiveException: WELD-001303: No active contexts for scope type javax.enterprise.context.RequestScoped
		Caused by: org.jboss.weld.context.ContextNotActiveException: WELD-001303: No active contexts for scope type javax.enterprise.context.RequestScoped"}}
	21:38:47,950 INFO  [org.jboss.as.server] (ServerService Thread Pool -- 34)  WFLYSRV0010: Deployed "welderror-0.1-SNAPSHOT.war" (runtime-name : "welderror-0.1-SNAPSHOT.war")
	21:38:47,955 INFO  [org.jboss.as.controller] (Controller Boot Thread)  WFLYCTL0183: Service status report
	WFLYCTL0186:   Services which failed to start:      service jboss.undertow.deployment.default-server.default-host./welderror: org.jboss.weld.context.ContextNotActiveException: WELD-001303: No active contexts for scope type javax.enterprise.context.RequestScoped
	
	21:38:48,009 INFO  [org.jboss.as.server] (Controller Boot Thread)  WFLYSRV0212: Resuming server
	21:38:48,011 INFO  [org.jboss.as] (Controller Boot Thread)  WFLYSRV0061: Http management interface listening on https://127.0.0.1:9993/management
	21:38:48,012 INFO  [org.jboss.as] (Controller Boot Thread)  WFLYSRV0052: Admin console listening on https://127.0.0.1:9993
	21:38:48,012 ERROR [org.jboss.as] (Controller Boot Thread)  WFLYSRV0026: JBoss EAP 7.1.0.GA (WildFly Core 3.0.10.Final-redhat-1) started (with errors) in 7421ms - Started 421 of 686 services (2 services failed or missing dependencies, 396 services are lazy, passive or on-demand)
